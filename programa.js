function cargarMapa() {
    let mapa = L.map('divMapa', { center: [4.625770, -74.172777], zoom: 5 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    let marcadorBOGOTA= L.marker([4.598, -74.0808333]);
    marcadorBOGOTA.addTo(mapa);

    let marcadorCali= L.marker([3.42158,  -76.5205]);
    marcadorCali.addTo(mapa);

    let marcadorCartagena= L.marker([10.383333,  -75.533333]);
    marcadorCartagena.addTo(mapa);



    


    

    var polygon = L.polygon([
        [4.7570666, -74.114191],
        [4.76223333, -74.01688611],
        [4.48823333, -74.0978444],
        [4.600663889,-74.1981667],
        [4.7570666, -74.114191],
    ]).addTo(mapa);

    var polygon = L.polygon([
        [3.51521944, -76.51912778],
        [3.49710, -76.4847],
        [3.4238, -76.4593],
        [3.3220, -76.5194],
        [3.4188, -76.5664],
        [3.51521944, -76.51912778],
    ]).addTo(mapa);


    var polygon = L.polygon([
        [10.4554, -75.5042666],
        [10.4188, -75.514530],
        [10.41249444, -75.47381944],
        [10.42565, -75.4521444],
        [10.38209167, -75.43547778],
        [10.3486, -75.485555556],
        [10.310775, -75.4811111],
        [10.31083333, -75.50388889],
        [10.385275, -75.5225],
        [10.3982833, -75.5653611],
        [10.4554, -75.5042666],
    ]).addTo(mapa);


}